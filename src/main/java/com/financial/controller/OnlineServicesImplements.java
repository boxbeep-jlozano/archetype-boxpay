package com.financial.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RestController;
import com.financial.models.Account;
import com.financial.models.Transaction;
import com.financial.util.Credentials;
import com.financial.util.DataInput;
import com.financial.util.GenericResponse;
import com.financial.util.Util;

@RestController()
public class OnlineServicesImplements implements OnlineServicesService {

	protected static final Logger LOGGER = Logger.getLogger(OnlineServicesImplements.class.getName());
	@Autowired
	private Environment env;

	@Override
	public GenericResponse openSession(DataInput dataInput) {
		Credentials credentials;
		try {
			credentials = Util.getCredentialsFromData(dataInput, env.getProperty("boxpay.privatekey"));
			LOGGER.info("User " + credentials.getUser());
			// Authenticacion con usuario=admin password=admin
			String jwtToken = Util.getJWTToken(credentials.getUser(), "6HDbB3kxMTmKceZz",
					Long.parseLong(env.getProperty("boxpay.timeSession").toString()));
			credentials.setToken(jwtToken);

			// Cifrar y retornar respuesta
			return Util.getOkResp(credentials, env.getProperty("boxpay.publickey"));

		} catch (Exception e2) {
			e2.printStackTrace();
			return Util.getErrorResp();
		}

	}

	@Override
	public GenericResponse queryAccount(DataInput dataInput) {
		Transaction transaction;
		try {
			LOGGER.info("Query Account");
			transaction = Util.getTransactionFromData(dataInput, env.getProperty("boxpay.privatekey"));
			LOGGER.info("Id Trasaccion " + transaction.getId());

			// CONSULTA DE CUENTAS
			Account account = new Account();
			account.setId("406000001"); //id, token o identificador de cuenta
			account.setNumber("4061XXXX123");
			BigDecimal bigDecimal = new BigDecimal(1000);
			account.setLimit_amount((bigDecimal.multiply(new BigDecimal(100))).longValue()); //monto maximo multiplicado por 100
			transaction.addAccount(account);

			// Cifrar y retornar respuesta
			return Util.getOkResp(transaction, env.getProperty("boxpay.publickey"));

		} catch (Exception e2) {
			e2.printStackTrace();
			return Util.getErrorResp();
		}

	}

	@Override
	public GenericResponse sendOtp(DataInput dataInput) {
		Transaction transaction;
		try {
			LOGGER.info("Send OTP");
			transaction = Util.getTransactionFromData(dataInput, env.getProperty("boxpay.privatekey"));
			LOGGER.info("Id Trasaccion " + transaction.getId());
			LOGGER.info("Account number " + transaction.getTransfer().getAccount_number());
			LOGGER.info("Amount " + transaction.getTransfer().getAmount()/100);

			// CONSULTA DE CUENTAS Y ENVIO DE OTP
			LOGGER.info("CODIGO ENVIADO AL CLIENTE");

			// Cifrar y retornar respuesta
			return Util.getOkResp(transaction, env.getProperty("boxpay.publickey"));

		} catch (Exception e2) {
			e2.printStackTrace();
			return Util.getErrorResp();
		}
	}

	@Override
	public GenericResponse requestTransfer(DataInput dataInput) {
		Transaction transaction;
		try {
			LOGGER.info("Request Transfer");
			transaction = Util.getTransactionFromData(dataInput, env.getProperty("boxpay.privatekey"));
			LOGGER.info("Id Trasaccion " + transaction.getId());
			LOGGER.info("Account number " + transaction.getTransfer().getAccount_number());
			LOGGER.info("Amount " + transaction.getTransfer().getAmount()/100);
			LOGGER.info("OTP " + transaction.getTransfer().getOtp_code());
			LOGGER.info("INTENTO " + transaction.getTransfer().getIntent());

			// CONSULTA DE CUENTAS Y NOTA DE CREDITO
			LOGGER.info("EJECUCION DE NOTA DE CREDITO");
			transaction.setDate_transfer(new Date().getTime());
			transaction.setAdditional_information("Informacion adicional");
			transaction.setProvider_reference("1234567890987654312");
			
			// Cifrar y retornar respuesta
			return Util.getOkResp(transaction, env.getProperty("boxpay.publickey"));

		} catch (Exception e2) {
			e2.printStackTrace();
			return Util.getErrorResp();
		}
	}

	@Override
	public GenericResponse reverseTransaction(DataInput dataInput) {
		Transaction transaction;
		try {
			LOGGER.info("Reverse Transaction");
			transaction = Util.getTransactionFromData(dataInput, env.getProperty("boxpay.privatekey"));
			LOGGER.info("Id Trasaccion " + transaction.getId());
			LOGGER.info("Account number " + transaction.getTransfer().getAccount_number());
			LOGGER.info("Amount " + transaction.getTransfer().getAmount()/100);
			LOGGER.info("PROVIDER_REFERENCE " + transaction.getProvider_reference());

			// CONSULTA DE TRANSACCION Y REVERSO
			LOGGER.info("EJECUCION DE REVERSO");
			transaction.setDate_reverse(new Date().getTime());
			transaction.setAdditional_information("Rerso de la trasaccion XXX");
			transaction.setProvider_reference("1234567890987654312");
			
			// Cifrar y retornar respuesta
			return Util.getOkResp(transaction, env.getProperty("boxpay.publickey"));

		} catch (Exception e2) {
			e2.printStackTrace();
			return Util.getErrorResp();
		}
	}

}
