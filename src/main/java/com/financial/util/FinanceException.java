package com.financial.util;

public class FinanceException  extends Exception {

	
	private static final long serialVersionUID = 1L;
	public FinanceException(String message) {
		super(message);
	}
	
	  public FinanceException(String message, Throwable cause) { super(message, cause); }

}
