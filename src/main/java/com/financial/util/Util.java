package com.financial.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.Normalizer;
import java.util.*;
import java.util.stream.Collectors;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.financial.models.Transaction;
import com.google.gson.Gson;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Util {

	public static PublicKey getPublicKey(String base64PublicKey) {
		PublicKey publicKey = null;
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			publicKey = keyFactory.generatePublic(keySpec);
			return publicKey;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return publicKey;
	}

	public static PrivateKey getPrivateKey(String base64PrivateKey) {
		PrivateKey privateKey = null;
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
		KeyFactory keyFactory = null;
		try {

			keyFactory = KeyFactory.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			privateKey = keyFactory.generatePrivate(keySpec);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return privateKey;
	}

	public static String decryptData(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException,
			NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return new String(cipher.doFinal(data));
	}

	public static byte[] encrypt(String data, String publicKey) throws BadPaddingException, IllegalBlockSizeException,
			InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
		return cipher.doFinal(data.getBytes());
	}

	public static String mapToJson(Map<String, Object> elements) throws JsonProcessingException {
		String json = new ObjectMapper().writeValueAsString(elements);
		return json;
	}

	static String getAlphaNumericString(int n) {

		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString.charAt(index));
		}
		return sb.toString();
	}

	public static GenericResponse getOkResp(Object data, String publicKey) {
		String encryptedData = "";
		String encryptedDataKey = "";
		String key = getAlphaNumericString(16);
		GenericResponse respuestaGenerica = new GenericResponse();
		try {
			Gson gson = new Gson();
			String data1 = gson.toJson(data);
			encryptedData = encryptAes(data1, key);
			encryptedDataKey = getEncrypt(key, publicKey);
			respuestaGenerica.setError("0");
			respuestaGenerica.setMensaje("PROCESADO CORRECTAMENTE");
			respuestaGenerica.setKey(encryptedDataKey);
			respuestaGenerica.setData(encryptedData);
		} catch (Exception e) {
			e.printStackTrace();
			respuestaGenerica.setError("-1");
			respuestaGenerica.setMensaje("Error de cifrado");
		}
		return respuestaGenerica;

	}

	public static GenericResponse getErrorResp() {
		GenericResponse respuestaGenerica = new GenericResponse();
		respuestaGenerica.setError("1");
		respuestaGenerica.setMensaje("ERROR");
		return respuestaGenerica;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public static String formatoTexto(String texto) {
		String formato = Normalizer.normalize(texto, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		formato = formato.replaceAll("[^A-Za-z0-9\\s]*", "");
		return formato;
	}

	public static Credentials getCredentialsFromData(DataInput dataInput, String base64PrivateKey)
			throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException,
			IllegalBlockSizeException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
		String key = Util.decryptData(Base64.getDecoder().decode(dataInput.getKey().getBytes()),
				Util.getPrivateKey(base64PrivateKey));
		String cyfredData = decryptAes(dataInput.getData(), key);
		Gson gson = new Gson();
		Credentials credentials = gson.fromJson(cyfredData, Credentials.class);
		return credentials;
	}

	public static Transaction getTransactionFromData(DataInput dataInput, String base64PrivateKey)
			throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException,
			IllegalBlockSizeException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
		String key = Util.decryptData(Base64.getDecoder().decode(dataInput.getKey().getBytes()),
				Util.getPrivateKey(base64PrivateKey));
		String cyfredData = decryptAes(dataInput.getData(), key);
		Gson gson = new Gson();
		Transaction transaction = gson.fromJson(cyfredData, Transaction.class);
		return transaction;

	}

	public static String getEncryptFromTransaction(Transaction transaction, String base64PublicKey)
			throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException,
			IllegalBlockSizeException {
		Gson gson = new Gson();
		byte[] bytes = Util.encrypt(gson.toJson(transaction), base64PublicKey);
		return Base64.getEncoder().encodeToString(bytes);

	}

	public static String getEncrypt(String credentials, String base64PublicKey) throws InvalidKeyException,
			NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
		byte[] bytes = Util.encrypt(credentials, base64PublicKey);
		return Base64.getEncoder().encodeToString(bytes);

	}

	public static String getEncryptFromCredentials(Object credentials, String base64PublicKey)
			throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException,
			IllegalBlockSizeException {
		Gson gson = new Gson();
		byte[] bytes = Util.encrypt(gson.toJson(credentials), base64PublicKey);
		return Base64.getEncoder().encodeToString(bytes);

	}

	public static String getJWTToken(String username, String secretKey, Long sessionTime) {
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("Default");

		String token = Jwts.builder().setId("softtekJWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + sessionTime))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

	// AES
	public static SecretKeySpec setKey(String myKey) {
		SecretKeySpec secretKey = null;
		byte[] key;
		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return secretKey;
	}

	public static String encryptAes(String strToEncrypt, String secret)
			throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		String encryptedText = "";

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		byte[] key = secret.getBytes("UTF-8");
		SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
		IvParameterSpec ivparameterspec = new IvParameterSpec(key);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivparameterspec);
		byte[] cipherText = cipher.doFinal(strToEncrypt.getBytes("UTF8"));
		Base64.Encoder encoder = Base64.getEncoder();
		encryptedText = encoder.encodeToString(cipherText);
		return encryptedText;
	}

	public static String decryptAes(String strToDecrypt, String secret)
			throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		byte[] key = secret.getBytes("UTF-8");
		SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
		IvParameterSpec ivparameterspec = new IvParameterSpec(key);
		cipher.init(Cipher.DECRYPT_MODE, secretKey, ivparameterspec);
		return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));

	}

}
