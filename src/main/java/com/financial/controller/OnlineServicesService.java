package com.financial.controller;

import javax.ws.rs.POST;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.financial.util.DataInput;
import com.financial.util.GenericResponse;


@RequestMapping("online_services")
@CrossOrigin
public interface OnlineServicesService {
	
	@POST
	@PostMapping("open_session")
	public GenericResponse openSession(@RequestBody DataInput dataInput);
	
	@POST
	@PostMapping("query_account")
	public GenericResponse queryAccount(@RequestBody DataInput dataInput);
	
	@POST
	@PostMapping("send_otp")
	public GenericResponse sendOtp(@RequestBody DataInput dataInput);
	
	@POST
	@PostMapping("request_transfer")
	public GenericResponse requestTransfer(@RequestBody DataInput dataInput);
	
	@POST
	@PostMapping("reverse_transaction")
	public GenericResponse reverseTransaction(@RequestBody DataInput dataInput);

}
