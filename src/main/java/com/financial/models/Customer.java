package com.financial.models;



//@JsonIgnoreProperties(ignoreUnknown = true)
//@Data
//@AllArgsConstructor
//@Entity
//@Table(name = "TABLE_NAME")
public class Customer {
	private String id;
	private String identification;
	private String name;
	private String email;
	private String phone;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentification() {
		return identification;
	}
	public void setIdentification(String identification) {
		this.identification = identification;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	
	
	
	
}