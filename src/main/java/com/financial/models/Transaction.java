package com.financial.models;

import java.util.ArrayList;
import java.util.List;

public class Transaction {

	private String id;
	private Long date;
	private Customer customer;
	private Commerce commerce;
	private Transfer transfer;
	private List<Account> accounts;
	private Long date_transfer;
	private Long date_reverse;
	private String additional_information;
	private String provider_reference;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getDate() {
		return date;
	}
	public void setDate(Long date) {
		this.date = date;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Commerce getCommerce() {
		return commerce;
	}
	public void setCommerce(Commerce commerce) {
		this.commerce = commerce;
	}
	public Transfer getTransfer() {
		return transfer;
	}
	public void setTransfer(Transfer transfer) {
		this.transfer = transfer;
	}
	public List<Account> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	
	public void addAccount(Account account) {
		if(this.accounts == null) {
			this.accounts = new ArrayList<Account>();
		}
		this.accounts.add(account);
	}
	public Long getDate_transfer() {
		return date_transfer;
	}
	public void setDate_transfer(Long date_transfer) {
		this.date_transfer = date_transfer;
	}
	public String getAdditional_information() {
		return additional_information;
	}
	public void setAdditional_information(String additional_information) {
		this.additional_information = additional_information;
	}
	public String getProvider_reference() {
		return provider_reference;
	}
	public void setProvider_reference(String provider_reference) {
		this.provider_reference = provider_reference;
	}
	public Long getDate_reverse() {
		return date_reverse;
	}
	public void setDate_reverse(Long date_reverse) {
		this.date_reverse = date_reverse;
	}
	
	
	
	
	
}