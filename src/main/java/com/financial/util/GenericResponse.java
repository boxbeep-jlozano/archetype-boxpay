package com.financial.util;



public class GenericResponse {
	private String error;
	private String mensaje;
	private String key;
	private Object data;
	
	
	public GenericResponse() {
		super();
	}
	public GenericResponse(String error, String mensaje, Object data) {
		super();
		this.error = error;
		this.mensaje = mensaje;
		this.data = data;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	
	
	

}
