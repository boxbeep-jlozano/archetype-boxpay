package com.financial.models;

public class Transfer {
	
	private String id;
	private Long date;
	private Long amount;
	private String reference;
	private String account_number;
	private int intent;
	private String otp_code;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getDate() {
		return date;
	}
	public void setDate(Long date) {
		this.date = date;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getAccount_number() {
		return account_number;
	}
	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}
	public String getOtp_code() {
		return otp_code;
	}
	public void setOtp_code(String otp_code) {
		this.otp_code = otp_code;
	}
	public int getIntent() {
		return intent;
	}
	public void setIntent(int intent) {
		this.intent = intent;
	}
	
	
	
}