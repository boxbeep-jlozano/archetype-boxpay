package com.financial.models;

import java.util.ArrayList;
import java.util.List;

public class TransactionResponse {

	private String id;
	private Long date;
	private List<Account> accounts;
	public List<Account> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getDate() {
		return date;
	}
	public void setDate(Long date) {
		this.date = date;
	}
	public void addAccount(Account account) {
		if(this.accounts == null) {
			this.accounts = new ArrayList<Account>();
		}
		this.accounts.add(account);
	}	
	
	
}